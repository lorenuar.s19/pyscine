class HotBeverages:
    def __init__(self, name = "hot beverage", price = 0.30, description = "Just some water in a cup"):
        self.name = name
        self.price = round(price, 2)
        self.description = description

    def __str__(self):
        str = ""
        str += "name : {}\n".format(self.name)
        str += "price : {:.2f}\n".format(self.price)
        str += "description : {}\n".format(self.description)
        return str

class Coffee(HotBeverages):
    def __init__(self):
        super().__init__()
        self.name = "coffee"
        self.price = 0.40
        self.description = "A coffee, to stay awake."

class Tea(HotBeverages):
    def __init__(self):
        super().__init__()
        self.name = "tea"

class Chocolate(HotBeverages):
    def __init__(self):
        super().__init__()
        self.name = "chocolate"
        self.price = 0.50
        self.description = "Chocolate, sweet chocolate..."

class Cappuccino(HotBeverages):
    def __init__(self):
        super().__init__()
        self.name = "cappuccino"
        self.price = 0.45
        self.description =  "Un po’ di Italia nella sua tazza!"

def main():
    print (HotBeverages())
    print (Coffee())
    print (Tea())
    print (Chocolate())
    print (Cappuccino())

if __name__ == '__main__':
    main()
