class Intern:
    def __init__(self, name = "My name? I’m nobody, an intern, I have no name."):
        self.name = name

    def __str__(self):
        return self.name

    class Coffee:
        def __str__(self):
            return "This is the worst coffee you ever tasted."

    def work(self):
        raise Exception("I’m just an intern, I can’t do that...")

    def make_coffee(self):
        return Intern.Coffee()


def main():
    no_name = Intern()
    print ("no_name :", no_name)
    print ("no_name : make_coffee :", no_name.make_coffee())
    print ("no_name : work : ", end='')
    try :
        no_name.work()
    except Exception as error:
        print (error)

    mark = Intern("Mark")
    print ("mark :", mark)
    print ("mark : make_coffee :", mark.make_coffee())
    print ("mark : work : ", end='')
    try :
        mark.work()
    except Exception as error:
        print (error)


if __name__ == '__main__':
    main()
