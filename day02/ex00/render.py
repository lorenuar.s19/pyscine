from settings import *
import sys, os, re

def check_args():
    if len(sys.argv) != 2:
        return 1
    else:
        return 0

def expand(line, out):

    try:
        tmp = line.format(**globals())
    except KeyError as key:
        print ("keyword not found :", key)
        sys.exit(1)
    except ValueError as val:
        print ("Value Error :", val)
        sys.exit(1)
    out.write(tmp)


def main():
    if check_args():
        exit (0)

    if not sys.argv[1].endswith('.template'):
        print ("Wrong file extension")
        sys.exit(1)
    try:
        with open("out.html", 'w') as out:
            with open(sys.argv[1], 'r') as reader:
                for line in reader:
                    expand(line, out)
    except IOError:
        return

if __name__ == '__main__':
    main()
