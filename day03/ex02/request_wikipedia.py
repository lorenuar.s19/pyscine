import requests
import dewiki
import json, sys

def main():
    if len(sys.argv) != 2:
        sys.exit(0)

    url = "https://en.wikipedia.org/w/api.php?action=parse&prop=wikitext&formatversion=2&format=json&redirects&page={0}".format(sys.argv[1])
    try :
        req = requests.get(url)
        print (req.history)
        resp = json.loads(req.text)
        result = dewiki.from_string(resp['parse']['wikitext'])
        with (open(sys.argv[1] + ".wiki",'w')) as output:
            output.write(result)
    except Exception as e:
        print ("Exception : ", e)
        sys.exit(1)

if __name__ == '__main__':
    main()
