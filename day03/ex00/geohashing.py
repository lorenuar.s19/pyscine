import sys, antigravity

def main():
    if len(sys.argv) != 3:
        sys.exit(0);
    try :
        antigravity.geohash(float(sys.argv[1]), float(sys.argv[2]), b'2005-05-26-10458.68')
    except Exception:
        sys.exit(1)

if __name__ == '__main__':
    main()
