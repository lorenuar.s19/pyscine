from path import Path
import sys

def exit_on_exception(e):
    print(e)
    sys.exit

def main():
    cwd = Path("./")
    cwd += "test_dir"
    try :
        print ("Creating " + cwd + " folder")
        Path.mkdir_p(cwd)
        cwd += "/test_file.txt"
        print ("Creating file " + cwd)
        Path.touch(cwd)
        print ("Writing text to file " + cwd)
        Path.write_text(cwd, "Lorem ipsum dolor sit amet", "UTF-8", append=False)
        print ("Reading text from " + cwd)
        print("data from " + cwd + "\n" + Path.read_text(cwd, "UTF-8"))
    except Exception as e:
        exit_on_exception(e)



if __name__ == '__main__':
    main()
