#!/bin/sh

PY='python3'

ENV_NAME='local_lib'

printf "\033[32;1m=== $PY -m pip --version ===\033[0m\n"
$PY -m pip --version

printf "\033[32;1m=== $PY -m venv ./$ENV_NAME ===\033[0m\n"
$PY -m venv ./$ENV_NAME

printf "\033[32;1m=== source $ENV_NAME/bin/activate ===\033[0m\n"
source $ENV_NAME/bin/activate

printf "\033[32;1m=== $PY -m pip install --log $ENV_NAME/installation.log git+https://github.com/jaraco/path.git pip --upgrade ===\033[0m\n"
$PY -m pip install --log $ENV_NAME/installation.log git+https://github.com/jaraco/path.git pip --upgrade

printf "\033[34;1m=== $PY my_program.py ===\033[0m\n"
$PY my_program.py
