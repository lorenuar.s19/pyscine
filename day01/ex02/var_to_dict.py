def list_to_dict(lst):
    dict_res = {}
    for i in range(0, len(lst), 1):
        name, year = lst[i]
        if year in dict_res:
            dict_res.update({year: dict_res.get(year) + " " + name})
        else :
            dict_res.update({year: name})
    return dict_res

def main () :
    d = [
    ('Hendrix' , '1942'),
    ('Allman' , '1946'),
    ('King' , '1925'),
    ('Clapton' , '1945'),
    ('Johnson' , '1911'),
    ('Berry' , '1926'),
    ('Vaughan' , '1954'),
    ('Cooder' , '1947'),
    ('Page' , '1944'),
    ('Richards' , '1943'),
    ('Hammett' , '1962'),
    ('Cobain' , '1967'),
    ('Garcia' , '1942'),
    ('Beck' , '1944'),
    ('Santana' , '1947'),
    ('Ramone' , '1948'),
    ('White' , '1975'),
    ('Frusciante', '1970'),
    ('Thompson' , '1949'),
    ('Burton' , '1939')
    ]

    dict_res = list_to_dict(d)

    for key, value in dict_res.items():
        print (key, ":", value)

if __name__ == '__main__':
    main()
