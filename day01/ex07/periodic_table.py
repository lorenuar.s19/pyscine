TABLE_WIDTH = 18
TABLE_HEIGHT = 7

class Block():
    def __init__(self):
        self.name = ""
        self.position = -1
        self.number = -1
        self.symbol = ""
        self.molar = 0.0
        self.electrons = ""

    def print_out(self):
        if self.position != -1 and self.number != -1:
            print (
                "Block : ", self,
                ", name :", self.name,
                ", position :", self.position,
                ", number : ", self.number,
                ", symbol :", self.symbol,
                ", molar :", self.molar,
                ", electrons", self.electrons)
        else:
            print("Block", "UNSET")

def render_to_html(Blocks):
    with open ("periodic_table.html", 'w') as html:
        html.write(
"""<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">

    <title>Periodic table of the elements</title>

<style>
        * :not(a) {
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
            background-color: #212121;
            color: #dddddd;
            font-size: small;
        }

        table,
        th,
        td {
            border: #dddddd solid thin;
            border-collapse: collapse;
        }

        ul,
        li {
            margin: auto;
            text-align: center;
            padding: .1vh .1vw;
            list-style-type:none;
        }

        th,
        td {
            padding: .25vh .25vw;
        }
    </style>

</head>

<body>

    <table>
""")
        html.write((" " * 4) + "<tr>\n")
        html.write((" " * 8) + "<th></th>\n")
        for x in range(TABLE_WIDTH):
            tmp = (" " * 8) + "<th>{0}</th>".format(x + 1)
            html.write(tmp)
            if x < TABLE_WIDTH:
                html.write('\n')
        html.write((" " * 4) + "</tr>\n")
        for y in range(TABLE_HEIGHT):
            html.write((" " * 4) + "<tr>\n")
            tmp = (" " * 8) + "<th>{0}</th>\n".format(y + 1)
            html.write(tmp)
            for x in range(TABLE_WIDTH):
                tmp = (" " * 8) + "<td>\n"
                if Blocks[x][y].position != -1:
                    tmp += (" " * 12) + "<h4 style=\"text-align : center\">{}</h4>\n".format(Blocks[x][y].name)
                    tmp += (" " * 12) + "<ul>\n"
                    tmp += (" " * 16) + "<li>No {}</li>\n".format(Blocks[x][y].number)
                    tmp += (" " * 16) + "<li>{}</li>\n".format(Blocks[x][y].symbol.strip())
                    tmp += (" " * 16) + "<li>{}</li>\n".format(Blocks[x][y].molar)
                    num_electrons = len(Blocks[x][y].electrons.split())
                    if num_electrons > 1:
                        tmp += (" " * 16) + "<li>{} electrons</li>\n".format(num_electrons)
                    else:
                        tmp += (" " * 16) + "<li>{} electron</li>\n".format(num_electrons)
                    tmp += (" " * 12) + "</ul>\n"
                tmp += (" " * 8) + "</td>\n"
                html.write(tmp)
            html.write((" " * 4) + "</tr>\n")
        html.write(
"""
    </table>

</body>

</html>
""")

def parse_input(reader):
    Blocks = [[Block() for x in range(TABLE_HEIGHT)] for y in range(TABLE_WIDTH)]
    for line in reader:
        lst = line.split(",")
        line_block = Block()
        for i in range(len(lst)):
            inp = lst[i]
            inp = inp.replace('\n', '')
            if i == 0:
                inp = inp.replace(' = ', ',')
                inp = inp.replace(':', ',')
                tmp = inp.split(',')
                for x in range(len(tmp)):
                    if x == 0:
                        line_block.name = tmp[x]
                    if "position" in tmp[x] and x < len(tmp):
                        line_block.position = int(tmp[x + 1])
            else :
                tmp = inp.split(':')
                if len(tmp) > 0:
                    if "number" in tmp[0]:
                        line_block.number = int(tmp[1])
                    elif "small" in tmp[0]:
                        line_block.symbol = tmp[1]
                    elif "molar" in tmp[0]:
                        line_block.molar = float(tmp[1])
                    elif "electron" in tmp[0]:
                        line_block.electrons = str(tmp[1:])
        x = line_block.position
        y = len(line_block.electrons.split(' ')) - 1
        Blocks[x][y] = line_block
    return Blocks

def main ( filename ) :
    Blocks = [[Block() for x in range(TABLE_HEIGHT)] for y in range(TABLE_WIDTH)]
    try:
        with open (filename, 'r') as reader :
            Blocks = parse_input(reader)
    except IOError:
        return
    render_to_html(Blocks)

if __name__ == '__main__':
    main("periodic_table.txt")
