import sys

def check_args():
    if len(sys.argv) != 2:
        return 1
    else:
        return 0

def main():
    if check_args():
        exit (0)

    states = {
        "Oregon" : "OR",
        "Alabama" : "AL",
        "New Jersey": "NJ",
        "Colorado" : "CO"
        }

    capital_cities = {
        "OR": "Salem",
        "AL": "Montgomery",
        "NJ": "Trenton",
        "CO": "Denver"
        }

    for key, value in capital_cities.items():
        if value == sys.argv[1]:
            for state, code in states.items():
                if code == key:
                    print(state)
                    return
    print("Unknown capital city")

if __name__ == '__main__':
    main()