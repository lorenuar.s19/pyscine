import sys

def check_args():
    if len(sys.argv) != 2:
        return 1
    else:
        return 0

def main():
    if check_args():
        exit (0)

    states = {
        "Oregon" : "OR",
        "Alabama" : "AL",
        "New Jersey": "NJ",
        "Colorado" : "CO"
        }

    capital_cities = {
        "OR": "Salem",
        "AL": "Montgomery",
        "NJ": "Trenton",
        "CO": "Denver"
        }
    if sys.argv[1] in states:
        print(capital_cities[states.get(sys.argv[1])])
    else:
        print("Unknown state")

if __name__ == '__main__':
    main()