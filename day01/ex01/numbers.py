def print_numbers_from_file ( filename ) :
    try :
        with open (filename, 'r') as reader :
            str = reader.read()
            for chr in str:
                if chr != ',':
                    print (chr, end='')
                else:
                    print('\n', end='')
    except IOError:
        return

if __name__ == '__main__':
    print_numbers_from_file("numbers.txt")
