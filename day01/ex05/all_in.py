import sys

def check_args():
    if len(sys.argv) != 2:
        return 1
    else:
        return 0

def normalize(inp):
    inp = inp.casefold()
    inp = ' '.join(inp.split())
    inp = inp.title()
    return inp

def search_key_normalized(dic, key):
    key = normalize(key)
    if key in dic:
       return dic.get(key)
    return ""

def search_value_normalized(dic, value):
    for key, data in dic.items():
        if data == value:
            return key, value
    return ("", "")

def search_for_input(inp):

    states = {
        "Oregon" : "OR",
        "Alabama" : "AL",
        "New Jersey": "NJ",
        "Colorado" : "CO"
        }

    capital_cities = {
        "OR": "Salem",
        "AL": "Montgomery",
        "NJ": "Trenton",
        "CO": "Denver"
        }

    out_state = ""
    out_capital = ""
    inp = normalize(inp)

    # if input is a state
    state = search_key_normalized(states, inp)
    if state != "" and state in capital_cities:
        out_capital = capital_cities.get(state)
    out_state = inp.strip()
    if state in capital_cities:
        out_capital = capital_cities.get(state)
    if state != "":
        return (out_state, out_capital)

    # if input is a capital city
    key, capital = search_value_normalized(capital_cities, inp)
    out_capital = capital
    key, state = search_value_normalized(states, key)
    out_state = key

    return (out_state, out_capital)

def check_input():
    inputs = sys.argv[1].split(',')

    for inp in inputs:
        if inp.strip() != "":
            state, capital = search_for_input(inp)
            if state != "" and capital != "":
                print(capital, "is the capital of", state)
            elif inp != "":
                print(inp.strip(), "is neither a capital city nor a state")

def main():
    if check_args():
        exit (0)
    check_input()

if __name__ == '__main__':
    main()