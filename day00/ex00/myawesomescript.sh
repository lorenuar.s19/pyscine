#!/bin/sh

if [[ $# -eq 1 ]]
then
    curl -s $1 | grep href= | cut -f 2 -d '"' | cut -f 1 -d '"'
fi